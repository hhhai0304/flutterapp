import 'package:flutter/material.dart';

import './product_control.dart';
import './products.dart';

class ProductManager extends StatefulWidget {
  final String startingProduct;

  ProductManager({this.startingProduct = 'Ok Tester'});

  @override
  State createState() {
    return ProductManagerState();
  }
}

class ProductManagerState extends State<ProductManager> {
  List<String> products = [];

  @override
  void initState() {
    super.initState();
    if (widget.startingProduct != null) {
      products.add(widget.startingProduct);
    }
  }

  void addProduct(String product) {
    setState(() {
      products.add(product);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            margin: EdgeInsets.all(10.0), child: ProductControl(addProduct)),
        Expanded(child: Products(products))
      ],
    );
  }
}
