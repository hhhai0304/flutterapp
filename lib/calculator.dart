import 'package:flutter/material.dart';

class Calculator extends StatefulWidget {
  @override
  State createState() {
    return CalculatorState();
  }
}

class CalculatorState extends State<Calculator> {
  TextEditingController controller1;
  TextEditingController controller2;
  String result = '';

  @override
  void initState() {
    super.initState();
    controller1 = TextEditingController(text: '');
    controller2 = TextEditingController(text: '');
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TextField(
          controller: controller1,
          decoration: InputDecoration(hintText: 'Nhập số thứ nhất')),
      TextField(
          controller: controller2,
          decoration: InputDecoration(hintText: 'Nhập số thứ nhất')),
      Text(result),
      RaisedButton(
        onPressed: () {
          setState(() {
            result = (int.parse(controller1.text) + int.parse(controller2.text))
                .toString();
          });
        },
        child: Text('Cộng'),
      )
    ]);
  }
}
